#!/usr/bin/env bash

set -e

function indent() {
  c='s/^/       /'
  case $(uname) in
    Darwin) sed -l "$c";;
    *)      sed -u "$c";;
  esac
}

# export all env-vars
if [ -d "$3" ]; then
  echo "Exporting environment variables" | indent

  for e in $(ls $3); do
    export "$e=$(cat $3/$e)"
  done
fi

unset GIT_DIR

currentdir=`dirname $BASH_SOURCE`

if [ -n "$SSH_KEYS" ]; then
  git config --global user.name 'Heroku Robot'
  git config --global user.email 'musicglue-heroku@musicglue.com'

  BUILDPACK_DIR=`pwd`
  BUILDPACK_GEM_HOME=$BUILDPACK_DIR/gems
  mkdir -p $BUILDPACK_GEM_HOME
  export GEM_HOME=$BUILDPACK_GEM_HOME
  export PATH="$GEM_HOME/bin:$PATH"
  export LANG=en_GB.utf-8

  gem install git-ssh-wrapper -v 0.2.0 > /dev/null

  $currentdir/install_ssh_keys.rb $1 | indent
fi

while IFS= read -r line; do
  dir=$(mktemp -t buildpackXXXXX)
  rm -rf $dir

  IFS=' ' read url key <<< "$line"
  IFS='#' read url branch <<< "$url"

  if [ "$url" != "" ]; then
    echo "-----> Downloading buildpack: $url"

    if [[ "$url" =~ \.tgz$ ]]; then
      mkdir -p "$dir"
      curl -s "$url" | tar xz -C "$dir" >/dev/null 2>&1
    else
      if [ "$key" == "" ]; then
        if [ "$MULTI_BUILDPACK_VERBOSE" == "true" ]; then
          git clone --verbose $url $dir
        else
          git clone $url $dir >/dev/null 2>&1
        fi
      else
        echo "Using ssh key: $key" | indent

        if [ "$MULTI_BUILDPACK_VERBOSE" == "true" ]; then
          echo "Downloading with: git-ssh $1/.ssh/keys/$key/id_rsa clone $url $dir" | indent
          git-ssh $1/.ssh/keys/$key/id_rsa clone $url $dir
        else
          git-ssh $1/.ssh/keys/$key/id_rsa clone $url $dir >/dev/null 2>&1
        fi
      fi

      if [ -f "$dir/.gitmodules" ]; then
        echo "Detected git submodules. Initializing..." | indent
        (cd $dir && git submodule update --init --recursive)
      fi
    fi

    cd $dir

    if [ "$branch" != "" ]; then
      echo "Checking out: $branch" | indent
      git checkout $branch >/dev/null 2>&1
    fi

    # we'll get errors later if these are needed and don't exist
    chmod -f +x $dir/bin/{detect,compile,release} || true

    set +e
    framework=$($dir/bin/detect $1)
    exitcode=$?
    set -e

    if [ $exitcode == 0 ]; then
      echo "$framework detected" | indent
      $dir/bin/compile $1 $2 $3

      if [ $? != 0 ]; then
        exit 1
      fi

      # check if the buildpack left behind an environment for subsequent ones
      if [ -e $dir/export ]; then
        source $dir/export
      fi

      if [ -x $dir/bin/release ]; then
        $dir/bin/release $1 > $1/last_pack_release.out
      fi
    fi
  fi
done < $1/.buildpacks

if [ -e $1/last_pack_release.out ]; then
  echo "Using release configuration from last framework $framework:" | indent
  cat $1/last_pack_release.out | indent
fi
