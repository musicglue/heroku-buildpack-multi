#!/usr/bin/env ruby

require 'net/http'
require 'openssl'
require 'uri'
require 'cgi'
require 'base64'
require 'open-uri'

puts "Installing ssh keys"

access_key = ENV['S3_ACCESS_KEY'] || ENV['AMAZON_ACCESS_KEY_ID']
secret_key = ENV['S3_SECRET_KEY'] || ENV['AMAZON_SECRET_ACCESS_KEY']

if access_key.to_s.empty? || secret_key.to_s.empty?
  puts "Couldn't find an S3 access & secret key pair in your environment."
  exit 1
end

def verbose_log message
  puts message if ENV['MULTI_BUILDPACK_VERBOSE'].to_s == "true"
end

def runit command
  verbose_log command
  `#{command}`
end

def write_ssh_key path, name, key
  key_dir = File.dirname path
  runit "mkdir -p #{key_dir} && chmod 700 #{key_dir}"
  File.open(path, "w+") { |f| f.write key }
  runit "chmod 400 #{path}"
end

def generate_download_uri bucket, path, access_key, secret_key
  verbose_log "Time now: #{Time.now.to_s}"
  expiry_date = Time.now.to_i + 30
  digest = OpenSSL::Digest.new 'sha1'
  string_to_sign = "GET\n\n\n#{expiry_date}\n/#{bucket}/#{path}"
  hmac = OpenSSL::HMAC.digest digest, secret_key, string_to_sign
  signature = CGI.escape Base64.encode64(hmac).strip
  URI("https://#{bucket}.s3.amazonaws.com/#{path}?AWSAccessKeyId=#{access_key}&Expires=#{expiry_date}&Signature=#{signature}")
end

def download_file uri
  open(uri) do |response|
    raise "Response code was #{response.code}, not 200" unless response.status[0] == "200"
    response.string
  end
end

build_ssh_dir = File.join ARGV[0], '.ssh'
ssh_keys = ENV['SSH_KEYS'].split.map(&:downcase)

ssh_keys.each do |key_name|
  bucket = 'musicglue-buildpacks'
  path = "ssh-keys/#{key_name}"

  begin
    uri = generate_download_uri bucket, path, access_key, secret_key
    verbose_log uri
    key = download_file uri
    key_dir = File.join build_ssh_dir, 'keys', key_name
    key_path = File.join key_dir, 'id_rsa'
    write_ssh_key key_path, key_name, key
  rescue => e
    puts "Couldn't download an ssh key called: #{key_name}"
    puts e.inspect
    exit 1
  end
end

if (ENV['INSTALL_SSH_KEYS_IN_HOME'] == 'true')
  home_keys_dir = "#{ENV['HOME']}/.ssh/keys"
  build_keys_dir = File.join build_ssh_dir, 'keys'
  build_config_path = File.join build_ssh_dir, 'config'

  ssh_keys.each do |key_name|
    relative_path = "#{key_name}/id_rsa"
    home_ssh_key_path = File.join home_keys_dir, relative_path
    build_ssh_key_path = File.join build_keys_dir, relative_path

    host_alias_env_var = "SSH_KEY_#{key_name.upcase}_HOST_ALIAS"
    host_alias = ENV[host_alias_env_var].to_s

    if host_alias.empty?
      puts "As INSTALL_SSH_KEYS_IN_HOME is true, you must also set #{host_alias_env_var}"
      exit 1
    end

    host_name_env_var = "SSH_KEY_#{key_name.upcase}_HOST_NAME"
    host_name = ENV[host_name_env_var].to_s

    if host_name.empty?
      puts "As INSTALL_SSH_KEYS_IN_HOME is true, you must also set #{host_name_env_var}"
      exit 1
    end

    File.open(build_config_path, "a+") do |f|
      f.write <<EOF
Host #{host_alias}
  HostName #{host_name}
  IdentityFile #{home_ssh_key_path}
  IdentityFile #{build_ssh_key_path}
  IdentitiesOnly true
  StrictHostKeyChecking no

EOF
    end

    runit "chmod 600 #{build_config_path}"
  end
end
